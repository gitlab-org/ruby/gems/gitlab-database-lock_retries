# frozen_string_literal: true

raise "Reserved for GitLab"

require_relative "lock_retries/version"

module Gitlab
  module Database
    module LockRetries
      class Error < StandardError; end
      # Your code goes here...
    end
  end
end
